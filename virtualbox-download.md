VirtualBox is a popular, free, and open-source virtualization software provided by Oracle. Here's how you can install and use VirtualBox on Linux:
Installation:

    Install from Package Manager: Most Linux distributions have VirtualBox in their repositories. For example:

        Ubuntu/Debian:

        bash

sudo apt update
sudo apt install virtualbox

Fedora:

bash

sudo dnf install VirtualBox

Arch Linux/Manjaro:

bash

    sudo pacman -S virtualbox

Install VirtualBox Extension Pack (Optional but recommended):
The Extension Pack adds useful features like USB 2.0/3.0 support, VirtualBox RDP, and PXE boot for Intel cards.

    First, download the extension pack from the official VirtualBox download page.
    Install it using:

    bash

    sudo VBoxManage extpack install <path_to_downloaded_extension_pack>

Add User to vboxusers Group:
To allow any user to access USB devices from VirtualBox VMs, you need to add that user to the vboxusers group.

bash

    sudo usermod -aG vboxusers $USER

Using VirtualBox:

    Launch VirtualBox:
    After installation, you can launch VirtualBox from your application menu or type virtualbox in the terminal.

    Create a New Virtual Machine:
        Click on "New" to start the VM creation wizard.
        Name your VM, choose the type (e.g., Linux, Windows), and the version (e.g., Ubuntu 64-bit).
        Allocate RAM and create a virtual hard drive as per your needs.
        Follow the prompts to finish VM creation.

    Install an Operating System:
    Once the VM is created, you'll need to install an OS on it.
        Click on your VM in VirtualBox and select "Settings".
        Under the "Storage" tab, mount the ISO file of the OS you want to install.
        Start the VM and follow the OS installation steps.

    Install Guest Additions (Recommended):
    Guest Additions provide better integration between the host OS and the guest OS, including improved display resolutions, seamless mouse integration, shared folders, and clipboard.
        Once your OS is installed, with the VM window active, go to "Devices" in the top menu and choose "Insert Guest Additions CD image..."
        Inside the VM, run the Guest Additions installer. The process might vary depending on the guest OS.

    Manage and Configure VMs:
    Using the VirtualBox interface, you can easily manage your VMs, take snapshots, clone VMs, modify VM settings, and much more.

Remember, VirtualBox also has comprehensive official documentation available online, which can be useful if you run into any issues or need more in-depth details about specific features.
